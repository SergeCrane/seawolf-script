# Generated from LabeledExpr.g4 by ANTLR 4.6
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .LabeledExprParser import LabeledExprParser
else:
    from LabeledExprParser import LabeledExprParser

# This class defines a complete generic visitor for a parse tree produced by LabeledExprParser.

class LabeledExprVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by LabeledExprParser#prog.
    def visitProg(self, ctx:LabeledExprParser.ProgContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#scope.
    def visitScope(self, ctx:LabeledExprParser.ScopeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#assign.
    def visitAssign(self, ctx:LabeledExprParser.AssignContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#indexAssign.
    def visitIndexAssign(self, ctx:LabeledExprParser.IndexAssignContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#ifStmt.
    def visitIfStmt(self, ctx:LabeledExprParser.IfStmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#whileLoopStmt.
    def visitWhileLoopStmt(self, ctx:LabeledExprParser.WhileLoopStmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#printExpr.
    def visitPrintExpr(self, ctx:LabeledExprParser.PrintExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#blank.
    def visitBlank(self, ctx:LabeledExprParser.BlankContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#block.
    def visitBlock(self, ctx:LabeledExprParser.BlockContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#ifstmt.
    def visitIfstmt(self, ctx:LabeledExprParser.IfstmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#whileLoop.
    def visitWhileLoop(self, ctx:LabeledExprParser.WhileLoopContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#LessGrtr.
    def visitLessGrtr(self, ctx:LabeledExprParser.LessGrtrContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#parens.
    def visitParens(self, ctx:LabeledExprParser.ParensContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#string.
    def visitString(self, ctx:LabeledExprParser.StringContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#Exponent.
    def visitExponent(self, ctx:LabeledExprParser.ExponentContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#AddSub.
    def visitAddSub(self, ctx:LabeledExprParser.AddSubContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#Index.
    def visitIndex(self, ctx:LabeledExprParser.IndexContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#real.
    def visitReal(self, ctx:LabeledExprParser.RealContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#FloorDiv.
    def visitFloorDiv(self, ctx:LabeledExprParser.FloorDivContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#list.
    def visitList(self, ctx:LabeledExprParser.ListContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#int.
    def visitInt(self, ctx:LabeledExprParser.IntContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#EqNeq.
    def visitEqNeq(self, ctx:LabeledExprParser.EqNeqContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#Not.
    def visitNot(self, ctx:LabeledExprParser.NotContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#emptyList.
    def visitEmptyList(self, ctx:LabeledExprParser.EmptyListContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#MulDivMod.
    def visitMulDivMod(self, ctx:LabeledExprParser.MulDivModContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#Contains.
    def visitContains(self, ctx:LabeledExprParser.ContainsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#id.
    def visitId(self, ctx:LabeledExprParser.IdContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#AndOr.
    def visitAndOr(self, ctx:LabeledExprParser.AndOrContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by LabeledExprParser#IndexError.
    def visitIndexError(self, ctx:LabeledExprParser.IndexErrorContext):
        return self.visitChildren(ctx)



del LabeledExprParser