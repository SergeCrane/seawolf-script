# Generated from LabeledExpr.g4 by ANTLR 4.6
# encoding: utf-8
from antlr4 import *
from io import StringIO

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\'")
        buf.write("\u008b\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3(\n")
        buf.write("\3\3\4\3\4\7\4,\n\4\f\4\16\4/\13\4\3\4\3\4\3\5\3\5\3\5")
        buf.write("\3\5\3\5\3\5\7\59\n\5\f\5\16\5<\13\5\3\5\3\5\5\5@\n\5")
        buf.write("\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\7")
        buf.write("\7O\n\7\f\7\16\7R\13\7\3\7\7\7U\n\7\f\7\16\7X\13\7\3\7")
        buf.write("\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7e\n\7\3\7")
        buf.write("\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3")
        buf.write("\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7")
        buf.write("\3\7\3\7\3\7\7\7\u0086\n\7\f\7\16\7\u0089\13\7\3\7\2\3")
        buf.write("\f\b\2\4\6\b\n\f\2\7\4\2\22\23\25\25\3\2\26\27\4\2\30")
        buf.write("\30\32\32\3\2\33\36\3\2 !\u00a0\2\16\3\2\2\2\4\'\3\2\2")
        buf.write("\2\6)\3\2\2\2\b\62\3\2\2\2\nA\3\2\2\2\fd\3\2\2\2\16\17")
        buf.write("\5\4\3\2\17\3\3\2\2\2\20(\5\6\4\2\21\22\7#\2\2\22\23\7")
        buf.write("\31\2\2\23\24\5\f\7\2\24\25\7\20\2\2\25(\3\2\2\2\26\27")
        buf.write("\7#\2\2\27\30\7\3\2\2\30\31\5\f\7\2\31\32\7\4\2\2\32\33")
        buf.write("\7\31\2\2\33\34\5\f\7\2\34\35\7\20\2\2\35(\3\2\2\2\36")
        buf.write("(\5\b\5\2\37(\5\n\6\2 !\7\5\2\2!\"\7\6\2\2\"#\5\f\7\2")
        buf.write("#$\7\7\2\2$%\7\20\2\2%(\3\2\2\2&(\7&\2\2\'\20\3\2\2\2")
        buf.write("\'\21\3\2\2\2\'\26\3\2\2\2\'\36\3\2\2\2\'\37\3\2\2\2\'")
        buf.write(" \3\2\2\2\'&\3\2\2\2(\5\3\2\2\2)-\7\b\2\2*,\5\4\3\2+*")
        buf.write("\3\2\2\2,/\3\2\2\2-+\3\2\2\2-.\3\2\2\2.\60\3\2\2\2/-\3")
        buf.write("\2\2\2\60\61\7\t\2\2\61\7\3\2\2\2\62\63\7\n\2\2\63\64")
        buf.write("\7\6\2\2\64\65\5\f\7\2\65\66\7\7\2\2\66:\5\6\4\2\679\7")
        buf.write("&\2\28\67\3\2\2\29<\3\2\2\2:8\3\2\2\2:;\3\2\2\2;?\3\2")
        buf.write("\2\2<:\3\2\2\2=>\7\13\2\2>@\5\6\4\2?=\3\2\2\2?@\3\2\2")
        buf.write("\2@\t\3\2\2\2AB\7\f\2\2BC\7\6\2\2CD\5\f\7\2DE\7\7\2\2")
        buf.write("EF\5\6\4\2F\13\3\2\2\2GH\b\7\1\2HI\7\37\2\2Ie\5\f\7\13")
        buf.write("JV\7\3\2\2KL\5\f\7\2LM\7\17\2\2MO\3\2\2\2NK\3\2\2\2OR")
        buf.write("\3\2\2\2PN\3\2\2\2PQ\3\2\2\2QS\3\2\2\2RP\3\2\2\2SU\5\f")
        buf.write("\7\2TP\3\2\2\2UX\3\2\2\2VT\3\2\2\2VW\3\2\2\2WY\3\2\2\2")
        buf.write("XV\3\2\2\2Ye\7\4\2\2Z[\7\3\2\2[e\7\4\2\2\\e\7\"\2\2]e")
        buf.write("\7$\2\2^e\7%\2\2_e\7#\2\2`a\7\6\2\2ab\5\f\7\2bc\7\7\2")
        buf.write("\2ce\3\2\2\2dG\3\2\2\2dJ\3\2\2\2dZ\3\2\2\2d\\\3\2\2\2")
        buf.write("d]\3\2\2\2d^\3\2\2\2d_\3\2\2\2d`\3\2\2\2e\u0087\3\2\2")
        buf.write("\2fg\f\22\2\2gh\t\2\2\2h\u0086\5\f\7\23ij\f\21\2\2jk\7")
        buf.write("\21\2\2k\u0086\5\f\7\22lm\f\20\2\2mn\7\24\2\2n\u0086\5")
        buf.write("\f\7\21op\f\17\2\2pq\t\3\2\2q\u0086\5\f\7\20rs\f\16\2")
        buf.write("\2st\7\16\2\2t\u0086\5\f\7\17uv\f\r\2\2vw\t\4\2\2w\u0086")
        buf.write("\5\f\7\16xy\f\f\2\2yz\t\5\2\2z\u0086\5\f\7\r{|\f\n\2\2")
        buf.write("|}\t\6\2\2}\u0086\5\f\7\13~\177\f\24\2\2\177\u0080\7\3")
        buf.write("\2\2\u0080\u0081\5\f\7\2\u0081\u0082\7\4\2\2\u0082\u0086")
        buf.write("\3\2\2\2\u0083\u0084\f\23\2\2\u0084\u0086\7\r\2\2\u0085")
        buf.write("f\3\2\2\2\u0085i\3\2\2\2\u0085l\3\2\2\2\u0085o\3\2\2\2")
        buf.write("\u0085r\3\2\2\2\u0085u\3\2\2\2\u0085x\3\2\2\2\u0085{\3")
        buf.write("\2\2\2\u0085~\3\2\2\2\u0085\u0083\3\2\2\2\u0086\u0089")
        buf.write("\3\2\2\2\u0087\u0085\3\2\2\2\u0087\u0088\3\2\2\2\u0088")
        buf.write("\r\3\2\2\2\u0089\u0087\3\2\2\2\13\'-:?PVd\u0085\u0087")
        return buf.getvalue()


class LabeledExprParser ( Parser ):

    grammarFileName = "LabeledExpr.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'['", "']'", "'print'", "'('", "')'", 
                     "'{'", "'}'", "'if'", "'else'", "'while'", "'[]'", 
                     "'in'", "','", "';'", "'**'", "'*'", "'/'", "'//'", 
                     "'%'", "'+'", "'-'", "'<>'", "'='", "'=='", "'<'", 
                     "'>'", "'<='", "'>='", "'not'", "'and'", "'or'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "SEMI", "EXP", "MUL", "DIV", 
                      "FLRDIV", "MOD", "ADD", "SUB", "NEQ", "EQ", "EQU", 
                      "LESS", "GRTR", "LEQ", "GEQ", "NOT", "AND", "OR", 
                      "REAL", "ID", "INT", "STRING", "NEWLINE", "WS" ]

    RULE_prog = 0
    RULE_stat = 1
    RULE_block = 2
    RULE_ifstmt = 3
    RULE_whileLoop = 4
    RULE_expr = 5

    ruleNames =  [ "prog", "stat", "block", "ifstmt", "whileLoop", "expr" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    SEMI=14
    EXP=15
    MUL=16
    DIV=17
    FLRDIV=18
    MOD=19
    ADD=20
    SUB=21
    NEQ=22
    EQ=23
    EQU=24
    LESS=25
    GRTR=26
    LEQ=27
    GEQ=28
    NOT=29
    AND=30
    OR=31
    REAL=32
    ID=33
    INT=34
    STRING=35
    NEWLINE=36
    WS=37

    def __init__(self, input:TokenStream):
        super().__init__(input)
        self.checkVersion("4.6")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class ProgContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def stat(self):
            return self.getTypedRuleContext(LabeledExprParser.StatContext,0)


        def getRuleIndex(self):
            return LabeledExprParser.RULE_prog

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProg" ):
                return visitor.visitProg(self)
            else:
                return visitor.visitChildren(self)




    def prog(self):

        localctx = LabeledExprParser.ProgContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_prog)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 12
            self.stat()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StatContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return LabeledExprParser.RULE_stat

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class BlankContext(StatContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.StatContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def NEWLINE(self):
            return self.getToken(LabeledExprParser.NEWLINE, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBlank" ):
                return visitor.visitBlank(self)
            else:
                return visitor.visitChildren(self)


    class IfStmtContext(StatContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.StatContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ifstmt(self):
            return self.getTypedRuleContext(LabeledExprParser.IfstmtContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIfStmt" ):
                return visitor.visitIfStmt(self)
            else:
                return visitor.visitChildren(self)


    class ScopeContext(StatContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.StatContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def block(self):
            return self.getTypedRuleContext(LabeledExprParser.BlockContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitScope" ):
                return visitor.visitScope(self)
            else:
                return visitor.visitChildren(self)


    class WhileLoopStmtContext(StatContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.StatContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def whileLoop(self):
            return self.getTypedRuleContext(LabeledExprParser.WhileLoopContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWhileLoopStmt" ):
                return visitor.visitWhileLoopStmt(self)
            else:
                return visitor.visitChildren(self)


    class IndexAssignContext(StatContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.StatContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(LabeledExprParser.ID, 0)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(LabeledExprParser.ExprContext)
            else:
                return self.getTypedRuleContext(LabeledExprParser.ExprContext,i)

        def EQ(self):
            return self.getToken(LabeledExprParser.EQ, 0)
        def SEMI(self):
            return self.getToken(LabeledExprParser.SEMI, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIndexAssign" ):
                return visitor.visitIndexAssign(self)
            else:
                return visitor.visitChildren(self)


    class AssignContext(StatContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.StatContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(LabeledExprParser.ID, 0)
        def EQ(self):
            return self.getToken(LabeledExprParser.EQ, 0)
        def expr(self):
            return self.getTypedRuleContext(LabeledExprParser.ExprContext,0)

        def SEMI(self):
            return self.getToken(LabeledExprParser.SEMI, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAssign" ):
                return visitor.visitAssign(self)
            else:
                return visitor.visitChildren(self)


    class PrintExprContext(StatContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.StatContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(LabeledExprParser.ExprContext,0)

        def SEMI(self):
            return self.getToken(LabeledExprParser.SEMI, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPrintExpr" ):
                return visitor.visitPrintExpr(self)
            else:
                return visitor.visitChildren(self)



    def stat(self):

        localctx = LabeledExprParser.StatContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_stat)
        try:
            self.state = 37
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,0,self._ctx)
            if la_ == 1:
                localctx = LabeledExprParser.ScopeContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 14
                self.block()
                pass

            elif la_ == 2:
                localctx = LabeledExprParser.AssignContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 15
                self.match(LabeledExprParser.ID)
                self.state = 16
                self.match(LabeledExprParser.EQ)
                self.state = 17
                self.expr(0)
                self.state = 18
                self.match(LabeledExprParser.SEMI)
                pass

            elif la_ == 3:
                localctx = LabeledExprParser.IndexAssignContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 20
                self.match(LabeledExprParser.ID)
                self.state = 21
                self.match(LabeledExprParser.T__0)
                self.state = 22
                self.expr(0)
                self.state = 23
                self.match(LabeledExprParser.T__1)
                self.state = 24
                self.match(LabeledExprParser.EQ)
                self.state = 25
                self.expr(0)
                self.state = 26
                self.match(LabeledExprParser.SEMI)
                pass

            elif la_ == 4:
                localctx = LabeledExprParser.IfStmtContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 28
                self.ifstmt()
                pass

            elif la_ == 5:
                localctx = LabeledExprParser.WhileLoopStmtContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 29
                self.whileLoop()
                pass

            elif la_ == 6:
                localctx = LabeledExprParser.PrintExprContext(self, localctx)
                self.enterOuterAlt(localctx, 6)
                self.state = 30
                self.match(LabeledExprParser.T__2)
                self.state = 31
                self.match(LabeledExprParser.T__3)
                self.state = 32
                self.expr(0)
                self.state = 33
                self.match(LabeledExprParser.T__4)
                self.state = 34
                self.match(LabeledExprParser.SEMI)
                pass

            elif la_ == 7:
                localctx = LabeledExprParser.BlankContext(self, localctx)
                self.enterOuterAlt(localctx, 7)
                self.state = 36
                self.match(LabeledExprParser.NEWLINE)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BlockContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def stat(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(LabeledExprParser.StatContext)
            else:
                return self.getTypedRuleContext(LabeledExprParser.StatContext,i)


        def getRuleIndex(self):
            return LabeledExprParser.RULE_block

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBlock" ):
                return visitor.visitBlock(self)
            else:
                return visitor.visitChildren(self)




    def block(self):

        localctx = LabeledExprParser.BlockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_block)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 39
            self.match(LabeledExprParser.T__5)
            self.state = 43
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << LabeledExprParser.T__2) | (1 << LabeledExprParser.T__5) | (1 << LabeledExprParser.T__7) | (1 << LabeledExprParser.T__9) | (1 << LabeledExprParser.ID) | (1 << LabeledExprParser.NEWLINE))) != 0):
                self.state = 40
                self.stat()
                self.state = 45
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 46
            self.match(LabeledExprParser.T__6)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class IfstmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self):
            return self.getTypedRuleContext(LabeledExprParser.ExprContext,0)


        def block(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(LabeledExprParser.BlockContext)
            else:
                return self.getTypedRuleContext(LabeledExprParser.BlockContext,i)


        def NEWLINE(self, i:int=None):
            if i is None:
                return self.getTokens(LabeledExprParser.NEWLINE)
            else:
                return self.getToken(LabeledExprParser.NEWLINE, i)

        def getRuleIndex(self):
            return LabeledExprParser.RULE_ifstmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIfstmt" ):
                return visitor.visitIfstmt(self)
            else:
                return visitor.visitChildren(self)




    def ifstmt(self):

        localctx = LabeledExprParser.IfstmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_ifstmt)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 48
            self.match(LabeledExprParser.T__7)
            self.state = 49
            self.match(LabeledExprParser.T__3)
            self.state = 50
            self.expr(0)
            self.state = 51
            self.match(LabeledExprParser.T__4)
            self.state = 52
            self.block()
            self.state = 56
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,2,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 53
                    self.match(LabeledExprParser.NEWLINE) 
                self.state = 58
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,2,self._ctx)

            self.state = 61
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==LabeledExprParser.T__8:
                self.state = 59
                self.match(LabeledExprParser.T__8)
                self.state = 60
                self.block()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class WhileLoopContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self):
            return self.getTypedRuleContext(LabeledExprParser.ExprContext,0)


        def block(self):
            return self.getTypedRuleContext(LabeledExprParser.BlockContext,0)


        def getRuleIndex(self):
            return LabeledExprParser.RULE_whileLoop

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWhileLoop" ):
                return visitor.visitWhileLoop(self)
            else:
                return visitor.visitChildren(self)




    def whileLoop(self):

        localctx = LabeledExprParser.WhileLoopContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_whileLoop)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 63
            self.match(LabeledExprParser.T__9)
            self.state = 64
            self.match(LabeledExprParser.T__3)
            self.state = 65
            self.expr(0)
            self.state = 66
            self.match(LabeledExprParser.T__4)
            self.state = 67
            self.block()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return LabeledExprParser.RULE_expr

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class LessGrtrContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.ExprContext
            super().__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(LabeledExprParser.ExprContext)
            else:
                return self.getTypedRuleContext(LabeledExprParser.ExprContext,i)

        def LESS(self):
            return self.getToken(LabeledExprParser.LESS, 0)
        def GRTR(self):
            return self.getToken(LabeledExprParser.GRTR, 0)
        def LEQ(self):
            return self.getToken(LabeledExprParser.LEQ, 0)
        def GEQ(self):
            return self.getToken(LabeledExprParser.GEQ, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLessGrtr" ):
                return visitor.visitLessGrtr(self)
            else:
                return visitor.visitChildren(self)


    class ParensContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(LabeledExprParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParens" ):
                return visitor.visitParens(self)
            else:
                return visitor.visitChildren(self)


    class StringContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def STRING(self):
            return self.getToken(LabeledExprParser.STRING, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitString" ):
                return visitor.visitString(self)
            else:
                return visitor.visitChildren(self)


    class ExponentContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(LabeledExprParser.ExprContext)
            else:
                return self.getTypedRuleContext(LabeledExprParser.ExprContext,i)

        def EXP(self):
            return self.getToken(LabeledExprParser.EXP, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExponent" ):
                return visitor.visitExponent(self)
            else:
                return visitor.visitChildren(self)


    class AddSubContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.ExprContext
            super().__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(LabeledExprParser.ExprContext)
            else:
                return self.getTypedRuleContext(LabeledExprParser.ExprContext,i)

        def ADD(self):
            return self.getToken(LabeledExprParser.ADD, 0)
        def SUB(self):
            return self.getToken(LabeledExprParser.SUB, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAddSub" ):
                return visitor.visitAddSub(self)
            else:
                return visitor.visitChildren(self)


    class IndexContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(LabeledExprParser.ExprContext)
            else:
                return self.getTypedRuleContext(LabeledExprParser.ExprContext,i)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIndex" ):
                return visitor.visitIndex(self)
            else:
                return visitor.visitChildren(self)


    class RealContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def REAL(self):
            return self.getToken(LabeledExprParser.REAL, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitReal" ):
                return visitor.visitReal(self)
            else:
                return visitor.visitChildren(self)


    class FloorDivContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(LabeledExprParser.ExprContext)
            else:
                return self.getTypedRuleContext(LabeledExprParser.ExprContext,i)

        def FLRDIV(self):
            return self.getToken(LabeledExprParser.FLRDIV, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFloorDiv" ):
                return visitor.visitFloorDiv(self)
            else:
                return visitor.visitChildren(self)


    class ListContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(LabeledExprParser.ExprContext)
            else:
                return self.getTypedRuleContext(LabeledExprParser.ExprContext,i)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitList" ):
                return visitor.visitList(self)
            else:
                return visitor.visitChildren(self)


    class IntContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def INT(self):
            return self.getToken(LabeledExprParser.INT, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInt" ):
                return visitor.visitInt(self)
            else:
                return visitor.visitChildren(self)


    class EqNeqContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.ExprContext
            super().__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(LabeledExprParser.ExprContext)
            else:
                return self.getTypedRuleContext(LabeledExprParser.ExprContext,i)

        def EQU(self):
            return self.getToken(LabeledExprParser.EQU, 0)
        def NEQ(self):
            return self.getToken(LabeledExprParser.NEQ, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEqNeq" ):
                return visitor.visitEqNeq(self)
            else:
                return visitor.visitChildren(self)


    class NotContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def NOT(self):
            return self.getToken(LabeledExprParser.NOT, 0)
        def expr(self):
            return self.getTypedRuleContext(LabeledExprParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNot" ):
                return visitor.visitNot(self)
            else:
                return visitor.visitChildren(self)


    class EmptyListContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEmptyList" ):
                return visitor.visitEmptyList(self)
            else:
                return visitor.visitChildren(self)


    class MulDivModContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.ExprContext
            super().__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(LabeledExprParser.ExprContext)
            else:
                return self.getTypedRuleContext(LabeledExprParser.ExprContext,i)

        def MUL(self):
            return self.getToken(LabeledExprParser.MUL, 0)
        def DIV(self):
            return self.getToken(LabeledExprParser.DIV, 0)
        def MOD(self):
            return self.getToken(LabeledExprParser.MOD, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMulDivMod" ):
                return visitor.visitMulDivMod(self)
            else:
                return visitor.visitChildren(self)


    class ContainsContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(LabeledExprParser.ExprContext)
            else:
                return self.getTypedRuleContext(LabeledExprParser.ExprContext,i)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitContains" ):
                return visitor.visitContains(self)
            else:
                return visitor.visitChildren(self)


    class IdContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(LabeledExprParser.ID, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitId" ):
                return visitor.visitId(self)
            else:
                return visitor.visitChildren(self)


    class AndOrContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.ExprContext
            super().__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(LabeledExprParser.ExprContext)
            else:
                return self.getTypedRuleContext(LabeledExprParser.ExprContext,i)

        def AND(self):
            return self.getToken(LabeledExprParser.AND, 0)
        def OR(self):
            return self.getToken(LabeledExprParser.OR, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAndOr" ):
                return visitor.visitAndOr(self)
            else:
                return visitor.visitChildren(self)


    class IndexErrorContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a LabeledExprParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(LabeledExprParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIndexError" ):
                return visitor.visitIndexError(self)
            else:
                return visitor.visitChildren(self)



    def expr(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = LabeledExprParser.ExprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 10
        self.enterRecursionRule(localctx, 10, self.RULE_expr, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 98
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,6,self._ctx)
            if la_ == 1:
                localctx = LabeledExprParser.NotContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 70
                self.match(LabeledExprParser.NOT)
                self.state = 71
                self.expr(9)
                pass

            elif la_ == 2:
                localctx = LabeledExprParser.ListContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 72
                self.match(LabeledExprParser.T__0)
                self.state = 84
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << LabeledExprParser.T__0) | (1 << LabeledExprParser.T__3) | (1 << LabeledExprParser.NOT) | (1 << LabeledExprParser.REAL) | (1 << LabeledExprParser.ID) | (1 << LabeledExprParser.INT) | (1 << LabeledExprParser.STRING))) != 0):
                    self.state = 78
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,4,self._ctx)
                    while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                        if _alt==1:
                            self.state = 73
                            self.expr(0)
                            self.state = 74
                            self.match(LabeledExprParser.T__12) 
                        self.state = 80
                        self._errHandler.sync(self)
                        _alt = self._interp.adaptivePredict(self._input,4,self._ctx)

                    self.state = 81
                    self.expr(0)
                    self.state = 86
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 87
                self.match(LabeledExprParser.T__1)
                pass

            elif la_ == 3:
                localctx = LabeledExprParser.EmptyListContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 88
                self.match(LabeledExprParser.T__0)
                self.state = 89
                self.match(LabeledExprParser.T__1)
                pass

            elif la_ == 4:
                localctx = LabeledExprParser.RealContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 90
                self.match(LabeledExprParser.REAL)
                pass

            elif la_ == 5:
                localctx = LabeledExprParser.IntContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 91
                self.match(LabeledExprParser.INT)
                pass

            elif la_ == 6:
                localctx = LabeledExprParser.StringContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 92
                self.match(LabeledExprParser.STRING)
                pass

            elif la_ == 7:
                localctx = LabeledExprParser.IdContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 93
                self.match(LabeledExprParser.ID)
                pass

            elif la_ == 8:
                localctx = LabeledExprParser.ParensContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 94
                self.match(LabeledExprParser.T__3)
                self.state = 95
                self.expr(0)
                self.state = 96
                self.match(LabeledExprParser.T__4)
                pass


            self._ctx.stop = self._input.LT(-1)
            self.state = 133
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,8,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 131
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,7,self._ctx)
                    if la_ == 1:
                        localctx = LabeledExprParser.MulDivModContext(self, LabeledExprParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 100
                        if not self.precpred(self._ctx, 16):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 16)")
                        self.state = 101
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << LabeledExprParser.MUL) | (1 << LabeledExprParser.DIV) | (1 << LabeledExprParser.MOD))) != 0)):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 102
                        self.expr(17)
                        pass

                    elif la_ == 2:
                        localctx = LabeledExprParser.ExponentContext(self, LabeledExprParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 103
                        if not self.precpred(self._ctx, 15):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 15)")
                        self.state = 104
                        self.match(LabeledExprParser.EXP)
                        self.state = 105
                        self.expr(16)
                        pass

                    elif la_ == 3:
                        localctx = LabeledExprParser.FloorDivContext(self, LabeledExprParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 106
                        if not self.precpred(self._ctx, 14):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 14)")
                        self.state = 107
                        self.match(LabeledExprParser.FLRDIV)
                        self.state = 108
                        self.expr(15)
                        pass

                    elif la_ == 4:
                        localctx = LabeledExprParser.AddSubContext(self, LabeledExprParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 109
                        if not self.precpred(self._ctx, 13):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 13)")
                        self.state = 110
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not(_la==LabeledExprParser.ADD or _la==LabeledExprParser.SUB):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 111
                        self.expr(14)
                        pass

                    elif la_ == 5:
                        localctx = LabeledExprParser.ContainsContext(self, LabeledExprParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 112
                        if not self.precpred(self._ctx, 12):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 12)")
                        self.state = 113
                        self.match(LabeledExprParser.T__11)
                        self.state = 114
                        self.expr(13)
                        pass

                    elif la_ == 6:
                        localctx = LabeledExprParser.EqNeqContext(self, LabeledExprParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 115
                        if not self.precpred(self._ctx, 11):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 11)")
                        self.state = 116
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not(_la==LabeledExprParser.NEQ or _la==LabeledExprParser.EQU):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 117
                        self.expr(12)
                        pass

                    elif la_ == 7:
                        localctx = LabeledExprParser.LessGrtrContext(self, LabeledExprParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 118
                        if not self.precpred(self._ctx, 10):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 10)")
                        self.state = 119
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << LabeledExprParser.LESS) | (1 << LabeledExprParser.GRTR) | (1 << LabeledExprParser.LEQ) | (1 << LabeledExprParser.GEQ))) != 0)):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 120
                        self.expr(11)
                        pass

                    elif la_ == 8:
                        localctx = LabeledExprParser.AndOrContext(self, LabeledExprParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 121
                        if not self.precpred(self._ctx, 8):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 8)")
                        self.state = 122
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not(_la==LabeledExprParser.AND or _la==LabeledExprParser.OR):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 123
                        self.expr(9)
                        pass

                    elif la_ == 9:
                        localctx = LabeledExprParser.IndexContext(self, LabeledExprParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 124
                        if not self.precpred(self._ctx, 18):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 18)")
                        self.state = 125
                        self.match(LabeledExprParser.T__0)
                        self.state = 126
                        self.expr(0)
                        self.state = 127
                        self.match(LabeledExprParser.T__1)
                        pass

                    elif la_ == 10:
                        localctx = LabeledExprParser.IndexErrorContext(self, LabeledExprParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 129
                        if not self.precpred(self._ctx, 17):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 17)")
                        self.state = 130
                        self.match(LabeledExprParser.T__10)
                        pass

             
                self.state = 135
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,8,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[5] = self.expr_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expr_sempred(self, localctx:ExprContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 16)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 15)
         

            if predIndex == 2:
                return self.precpred(self._ctx, 14)
         

            if predIndex == 3:
                return self.precpred(self._ctx, 13)
         

            if predIndex == 4:
                return self.precpred(self._ctx, 12)
         

            if predIndex == 5:
                return self.precpred(self._ctx, 11)
         

            if predIndex == 6:
                return self.precpred(self._ctx, 10)
         

            if predIndex == 7:
                return self.precpred(self._ctx, 8)
         

            if predIndex == 8:
                return self.precpred(self._ctx, 18)
         

            if predIndex == 9:
                return self.precpred(self._ctx, 17)
         




