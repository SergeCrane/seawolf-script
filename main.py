import sys
from antlr4 import *
from antlr4.InputStream import InputStream
from antlr4.error.ErrorListener import ErrorListener
from LabeledExprLexer import LabeledExprLexer
from LabeledExprParser import LabeledExprParser
from MyVisitor import MyVisitor

error_list = []

class MyErrorListener(ErrorListener):
    def __init__(self):
        super(MyErrorListener, self).__init__()

    def syntaxError(self, recognizer, offendingSymbol, line, column, msg, e):
        error_list.append(int(line))


if __name__ == '__main__':
    if len(sys.argv) > 1:
        input_stream = FileStream(sys.argv[1])
    else:
        input_stream = InputStream(sys.stdin.readline())

    lexer = LabeledExprLexer(input_stream)
    token_stream = CommonTokenStream(lexer)
    parser = LabeledExprParser(token_stream)
    parser._listeners = [MyErrorListener()]
    tree = parser.prog()

    visitor = MyVisitor()
    visitor.setErrors(error_list)

    if not error_list:
        visitor.visit(tree)
    else:
        print("SYNTAX ERROR")
