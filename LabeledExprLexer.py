# Generated from LabeledExpr.g4 by ANTLR 4.6
from antlr4 import *
from io import StringIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\'")
        buf.write("\u00e1\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%")
        buf.write("\4&\t&\3\2\3\2\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5")
        buf.write("\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3")
        buf.write("\n\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\r\3\r\3")
        buf.write("\r\3\16\3\16\3\17\3\17\3\20\3\20\3\20\3\21\3\21\3\22\3")
        buf.write("\22\3\23\3\23\3\23\3\24\3\24\3\25\3\25\3\26\3\26\3\27")
        buf.write("\3\27\3\27\3\30\3\30\3\31\3\31\3\31\3\32\3\32\3\33\3\33")
        buf.write("\3\34\3\34\3\34\3\35\3\35\3\35\3\36\3\36\3\36\3\36\3\37")
        buf.write("\3\37\3\37\3\37\3 \3 \3 \3!\7!\u00a6\n!\f!\16!\u00a9\13")
        buf.write("!\3!\3!\6!\u00ad\n!\r!\16!\u00ae\3!\6!\u00b2\n!\r!\16")
        buf.write("!\u00b3\3!\3!\7!\u00b8\n!\f!\16!\u00bb\13!\5!\u00bd\n")
        buf.write("!\3\"\3\"\7\"\u00c1\n\"\f\"\16\"\u00c4\13\"\3#\6#\u00c7")
        buf.write("\n#\r#\16#\u00c8\3$\3$\3$\3$\7$\u00cf\n$\f$\16$\u00d2")
        buf.write("\13$\3$\3$\3%\5%\u00d7\n%\3%\3%\3&\6&\u00dc\n&\r&\16&")
        buf.write("\u00dd\3&\3&\2\2\'\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n")
        buf.write("\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'")
        buf.write("\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36;\37= ")
        buf.write("?!A\"C#E$G%I&K\'\3\2\b\3\2\62;\4\2C\\c|\6\2\62;C\\aac")
        buf.write("|\n\2$$))^^ddhhppttvv\6\2\f\f\17\17$$^^\4\2\13\13\"\"")
        buf.write("\u00eb\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2")
        buf.write("\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2")
        buf.write("\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33")
        buf.write("\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2")
        buf.write("\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2")
        buf.write("\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2")
        buf.write("\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2")
        buf.write("\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3")
        buf.write("\2\2\2\2K\3\2\2\2\3M\3\2\2\2\5O\3\2\2\2\7Q\3\2\2\2\tW")
        buf.write("\3\2\2\2\13Y\3\2\2\2\r[\3\2\2\2\17]\3\2\2\2\21_\3\2\2")
        buf.write("\2\23b\3\2\2\2\25g\3\2\2\2\27m\3\2\2\2\31p\3\2\2\2\33")
        buf.write("s\3\2\2\2\35u\3\2\2\2\37w\3\2\2\2!z\3\2\2\2#|\3\2\2\2")
        buf.write("%~\3\2\2\2\'\u0081\3\2\2\2)\u0083\3\2\2\2+\u0085\3\2\2")
        buf.write("\2-\u0087\3\2\2\2/\u008a\3\2\2\2\61\u008c\3\2\2\2\63\u008f")
        buf.write("\3\2\2\2\65\u0091\3\2\2\2\67\u0093\3\2\2\29\u0096\3\2")
        buf.write("\2\2;\u0099\3\2\2\2=\u009d\3\2\2\2?\u00a1\3\2\2\2A\u00bc")
        buf.write("\3\2\2\2C\u00be\3\2\2\2E\u00c6\3\2\2\2G\u00ca\3\2\2\2")
        buf.write("I\u00d6\3\2\2\2K\u00db\3\2\2\2MN\7]\2\2N\4\3\2\2\2OP\7")
        buf.write("_\2\2P\6\3\2\2\2QR\7r\2\2RS\7t\2\2ST\7k\2\2TU\7p\2\2U")
        buf.write("V\7v\2\2V\b\3\2\2\2WX\7*\2\2X\n\3\2\2\2YZ\7+\2\2Z\f\3")
        buf.write("\2\2\2[\\\7}\2\2\\\16\3\2\2\2]^\7\177\2\2^\20\3\2\2\2")
        buf.write("_`\7k\2\2`a\7h\2\2a\22\3\2\2\2bc\7g\2\2cd\7n\2\2de\7u")
        buf.write("\2\2ef\7g\2\2f\24\3\2\2\2gh\7y\2\2hi\7j\2\2ij\7k\2\2j")
        buf.write("k\7n\2\2kl\7g\2\2l\26\3\2\2\2mn\7]\2\2no\7_\2\2o\30\3")
        buf.write("\2\2\2pq\7k\2\2qr\7p\2\2r\32\3\2\2\2st\7.\2\2t\34\3\2")
        buf.write("\2\2uv\7=\2\2v\36\3\2\2\2wx\7,\2\2xy\7,\2\2y \3\2\2\2")
        buf.write("z{\7,\2\2{\"\3\2\2\2|}\7\61\2\2}$\3\2\2\2~\177\7\61\2")
        buf.write("\2\177\u0080\7\61\2\2\u0080&\3\2\2\2\u0081\u0082\7\'\2")
        buf.write("\2\u0082(\3\2\2\2\u0083\u0084\7-\2\2\u0084*\3\2\2\2\u0085")
        buf.write("\u0086\7/\2\2\u0086,\3\2\2\2\u0087\u0088\7>\2\2\u0088")
        buf.write("\u0089\7@\2\2\u0089.\3\2\2\2\u008a\u008b\7?\2\2\u008b")
        buf.write("\60\3\2\2\2\u008c\u008d\7?\2\2\u008d\u008e\7?\2\2\u008e")
        buf.write("\62\3\2\2\2\u008f\u0090\7>\2\2\u0090\64\3\2\2\2\u0091")
        buf.write("\u0092\7@\2\2\u0092\66\3\2\2\2\u0093\u0094\7>\2\2\u0094")
        buf.write("\u0095\7?\2\2\u00958\3\2\2\2\u0096\u0097\7@\2\2\u0097")
        buf.write("\u0098\7?\2\2\u0098:\3\2\2\2\u0099\u009a\7p\2\2\u009a")
        buf.write("\u009b\7q\2\2\u009b\u009c\7v\2\2\u009c<\3\2\2\2\u009d")
        buf.write("\u009e\7c\2\2\u009e\u009f\7p\2\2\u009f\u00a0\7f\2\2\u00a0")
        buf.write(">\3\2\2\2\u00a1\u00a2\7q\2\2\u00a2\u00a3\7t\2\2\u00a3")
        buf.write("@\3\2\2\2\u00a4\u00a6\t\2\2\2\u00a5\u00a4\3\2\2\2\u00a6")
        buf.write("\u00a9\3\2\2\2\u00a7\u00a5\3\2\2\2\u00a7\u00a8\3\2\2\2")
        buf.write("\u00a8\u00aa\3\2\2\2\u00a9\u00a7\3\2\2\2\u00aa\u00ac\7")
        buf.write("\60\2\2\u00ab\u00ad\t\2\2\2\u00ac\u00ab\3\2\2\2\u00ad")
        buf.write("\u00ae\3\2\2\2\u00ae\u00ac\3\2\2\2\u00ae\u00af\3\2\2\2")
        buf.write("\u00af\u00bd\3\2\2\2\u00b0\u00b2\t\2\2\2\u00b1\u00b0\3")
        buf.write("\2\2\2\u00b2\u00b3\3\2\2\2\u00b3\u00b1\3\2\2\2\u00b3\u00b4")
        buf.write("\3\2\2\2\u00b4\u00b5\3\2\2\2\u00b5\u00b9\7\60\2\2\u00b6")
        buf.write("\u00b8\t\2\2\2\u00b7\u00b6\3\2\2\2\u00b8\u00bb\3\2\2\2")
        buf.write("\u00b9\u00b7\3\2\2\2\u00b9\u00ba\3\2\2\2\u00ba\u00bd\3")
        buf.write("\2\2\2\u00bb\u00b9\3\2\2\2\u00bc\u00a7\3\2\2\2\u00bc\u00b1")
        buf.write("\3\2\2\2\u00bdB\3\2\2\2\u00be\u00c2\t\3\2\2\u00bf\u00c1")
        buf.write("\t\4\2\2\u00c0\u00bf\3\2\2\2\u00c1\u00c4\3\2\2\2\u00c2")
        buf.write("\u00c0\3\2\2\2\u00c2\u00c3\3\2\2\2\u00c3D\3\2\2\2\u00c4")
        buf.write("\u00c2\3\2\2\2\u00c5\u00c7\t\2\2\2\u00c6\u00c5\3\2\2\2")
        buf.write("\u00c7\u00c8\3\2\2\2\u00c8\u00c6\3\2\2\2\u00c8\u00c9\3")
        buf.write("\2\2\2\u00c9F\3\2\2\2\u00ca\u00d0\7$\2\2\u00cb\u00cc\7")
        buf.write("^\2\2\u00cc\u00cf\t\5\2\2\u00cd\u00cf\n\6\2\2\u00ce\u00cb")
        buf.write("\3\2\2\2\u00ce\u00cd\3\2\2\2\u00cf\u00d2\3\2\2\2\u00d0")
        buf.write("\u00ce\3\2\2\2\u00d0\u00d1\3\2\2\2\u00d1\u00d3\3\2\2\2")
        buf.write("\u00d2\u00d0\3\2\2\2\u00d3\u00d4\7$\2\2\u00d4H\3\2\2\2")
        buf.write("\u00d5\u00d7\7\17\2\2\u00d6\u00d5\3\2\2\2\u00d6\u00d7")
        buf.write("\3\2\2\2\u00d7\u00d8\3\2\2\2\u00d8\u00d9\7\f\2\2\u00d9")
        buf.write("J\3\2\2\2\u00da\u00dc\t\7\2\2\u00db\u00da\3\2\2\2\u00dc")
        buf.write("\u00dd\3\2\2\2\u00dd\u00db\3\2\2\2\u00dd\u00de\3\2\2\2")
        buf.write("\u00de\u00df\3\2\2\2\u00df\u00e0\b&\2\2\u00e0L\3\2\2\2")
        buf.write("\16\2\u00a7\u00ae\u00b3\u00b9\u00bc\u00c2\u00c8\u00ce")
        buf.write("\u00d0\u00d6\u00dd\3\b\2\2")
        return buf.getvalue()


class LabeledExprLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]


    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    T__8 = 9
    T__9 = 10
    T__10 = 11
    T__11 = 12
    T__12 = 13
    SEMI = 14
    EXP = 15
    MUL = 16
    DIV = 17
    FLRDIV = 18
    MOD = 19
    ADD = 20
    SUB = 21
    NEQ = 22
    EQ = 23
    EQU = 24
    LESS = 25
    GRTR = 26
    LEQ = 27
    GEQ = 28
    NOT = 29
    AND = 30
    OR = 31
    REAL = 32
    ID = 33
    INT = 34
    STRING = 35
    NEWLINE = 36
    WS = 37

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'['", "']'", "'print'", "'('", "')'", "'{'", "'}'", "'if'", 
            "'else'", "'while'", "'[]'", "'in'", "','", "';'", "'**'", "'*'", 
            "'/'", "'//'", "'%'", "'+'", "'-'", "'<>'", "'='", "'=='", "'<'", 
            "'>'", "'<='", "'>='", "'not'", "'and'", "'or'" ]

    symbolicNames = [ "<INVALID>",
            "SEMI", "EXP", "MUL", "DIV", "FLRDIV", "MOD", "ADD", "SUB", 
            "NEQ", "EQ", "EQU", "LESS", "GRTR", "LEQ", "GEQ", "NOT", "AND", 
            "OR", "REAL", "ID", "INT", "STRING", "NEWLINE", "WS" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", 
                  "T__7", "T__8", "T__9", "T__10", "T__11", "T__12", "SEMI", 
                  "EXP", "MUL", "DIV", "FLRDIV", "MOD", "ADD", "SUB", "NEQ", 
                  "EQ", "EQU", "LESS", "GRTR", "LEQ", "GEQ", "NOT", "AND", 
                  "OR", "REAL", "ID", "INT", "STRING", "NEWLINE", "WS" ]

    grammarFileName = "LabeledExpr.g4"

    def __init__(self, input=None):
        super().__init__(input)
        self.checkVersion("4.6")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


