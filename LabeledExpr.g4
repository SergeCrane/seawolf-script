grammar LabeledExpr;

prog:   stat
    ;

stat:   block                                        # scope
    |   ID EQ expr SEMI                              # assign
    |   ID '[' expr ']' EQ expr SEMI                 # indexAssign
    |   ifstmt                                       # ifStmt
    |   whileLoop                                    # whileLoopStmt
    |   'print' '('expr')' SEMI                      # printExpr
    |   NEWLINE                                      # blank
    ;

block:  '{' stat* '}'
    ;

ifstmt: 'if' '('expr')' block NEWLINE* ('else' block)?
    ;

whileLoop: 'while' '('expr')' block
    ;

expr:   expr '[' expr ']'                   # Index
    |   expr '[]'                           # IndexError
    |   expr op=(MUL|DIV|MOD) expr          # MulDivMod
    |   expr EXP expr                       # Exponent
    |   expr FLRDIV expr                    # FloorDiv
    |   expr op=(ADD|SUB) expr              # AddSub
    |   expr 'in' expr                      # Contains
    |   expr op=(EQU|NEQ) expr              # EqNeq
    |   expr op=(LESS|GRTR|LEQ|GEQ) expr    # LessGrtr
    |   NOT  expr                           # Not
    |   expr op=(AND|OR) expr               # AndOr
    |   '[' ((expr',')* expr)* ']'          # list
    |   '[' ']'                             # emptyList
    |   REAL                                # real
    |   INT                                 # int
    |   STRING                              # string
    |   ID                                  # id
    |   '(' expr ')'                        # parens
    ;

SEMI:   ';' ;
EXP :   '**' ;
MUL :   '*' ; // assigns token name to '*' used above in grammar
DIV :   '/' ;
FLRDIV: '//' ;
MOD :   '%' ;
ADD :   '+' ;
SUB :   '-' ;
NEQ :   '<>' ;
EQ  :   '=' ;
EQU  :  '==' ;
LESS:   '<' ;
GRTR:   '>' ;
LEQ :   '<=' ;
GEQ :   '>=' ;
NOT :   'not' ;
AND :   'and' ;
OR  :   'or' ;
REAL:   [0-9]*'.'[0-9]+|[0-9]+'.'[0-9]* ;
ID  :   [A-Za-z][A-Za-z0-9_]* ;                 // match identifiers
INT :   [0-9]+ ;                                // match integers
STRING: '"'('\\'[btnfr"'\\] | ~[\r\n\\"])*'"' ;
NEWLINE:'\r'? '\n' ;     // return newlines to parser (is end-statement signal)
WS  :   [ \t]+ -> skip ; // toss out whitespace
