import sys
from LabeledExprVisitor import LabeledExprVisitor
from LabeledExprParser import LabeledExprParser




def lrHelper(left, right):
    if isinstance(left, str):
        try:
            if '.' in left:
                left = float(left)
            else:
                left = int(left)
        except ValueError:
            left = left.replace('"', '')

    if isinstance(right, str):
        try:
            if '.' in right:
                right = float(right)
            else:
                right = int(right)
        except ValueError:
            right = right.replace('"', '')
    return left, right


def strHelper(s):
    if isinstance(s, str):
        try:
            if '.' in s:
                s = float(s)
            else:
                s = int(s)
        except ValueError:
            s = s.replace('"', '')
    return s


class MyVisitor(LabeledExprVisitor):
    def __init__(self):
        self.memory = {}
        self.linectr = 1
        self.errors = []
        self.printing = True
        self.lastctr = 0

    def setErrors(self, error_list):
        self.errors = error_list

    def visitScope(self, ctx):
        return self.visit(ctx.block())

    def visitIfstmt(self, ctx):
        condition = self.visit(ctx.expr())
        condition = strHelper(condition)
        if condition != 0:
            return self.visit(ctx.block(0))
        else:
            try:
                return self.visit(ctx.block(1))
            except:
                return

    def visitWhileLoop(self, ctx):
        while True:
            condition = self.visit(ctx.expr())
            condition = strHelper(condition)
            if condition != 0:
                self.visit(ctx.block())
            else:
                return

    def visitAssign(self, ctx):
        name = ctx.ID().getText()
        value = self.visit(ctx.expr())
        value = strHelper(value)
        self.linectr += 1
        try:
            self.memory[name] = value
            return value
        except TypeError:
            return

    def visitIndex(self, ctx):
        name = strHelper(self.visit(ctx.expr(0)))
        index = self.visit(ctx.expr(1))
        index = int(index)
        self.linectr += 1
        if index is None:
            return
        try:
            if isinstance(name, list):
                return name[index]
            else:
                return self.memory[name][index]
        except:
            return

    def visitIndexError(self, ctx):
        self.linectr += 1
        return

    def visitIndexAssign(self, ctx):
        name = ctx.ID().getText()
        index = self.visit(ctx.expr(0))
        index = int(index)
        toAssign = strHelper(self.visit(ctx.expr(1)))

        self.linectr += 1

        try:
            self.memory[name][index] = toAssign
            return toAssign
        except IndexError:
            print('SEMANTIC ERROR')
            return

    def visitContains(self, ctx):
        this = self.visit(ctx.expr(0))
        that = self.visit(ctx.expr(1))
        this, that = strHelper(this), strHelper(that)

        try:
            if this in that:
                return 1
            else:
                return 0
        except TypeError:
            return

    def visitPrintExpr(self, ctx):
        value = self.visit(ctx.expr())
        if self.linectr > self.lastctr:
            self.printing = True
        if self.linectr in self.errors:
            print("SYNTAX ERROR LINE " + str(self.linectr))
            while self.linectr in self.errors:
                self.errors.remove(self.linectr)
            self.printing = False
            return
        if value is None:
            print("SEMANTIC ERROR")
            self.linectr += 1
            sys.exit()
        if self.printing:
            sys.stdout.write(str(strHelper(value)))
            #print(repr())
            self.linectr += 1

    def visitBlank(self, ctx):
        self.printing = True

    def visitString(self, ctx):
        s = '' + ctx.STRING().getText()
        return s

    def visitReal(self, ctx):
        return ctx.REAL().getText()

    def visitInt(self, ctx):
        return ctx.INT().getText()

    def visitId(self, ctx):
        name = ctx.ID().getText()
        if name in self.memory:
            return self.memory[name]
        return 0

    def visitParens(self, ctx):
        return self.visit(ctx.expr())

    def visitList(self, ctx):
        list = []
        exprs = ctx.expr()

        for i in range(0, len(exprs)):
            element = strHelper(self.visit(ctx.expr(i)))
            list.append(element)

        return list

    def visitEmptyList(self, ctx):
        return []

    #def visitNegate(self, ctx):
    #    value = strHelper(self.visit(ctx.expr()))
    #    if isinstance(value, str):
    #        return
    #    else:
    #        return -value

    def visitExponent(self, ctx):
        left = self.visit(ctx.expr(0))
        right = self.visit(ctx.expr(1))
        left, right = lrHelper(left, right)

        if isinstance(left, int) or isinstance(left, float):
            if isinstance(right, int) or isinstance(right, float):
                return left ** right
        else:
            return

    def visitMulDivMod(self, ctx):
        left = self.visit(ctx.expr(0))
        right = self.visit(ctx.expr(1))
        left, right = lrHelper(left, right)

        #if isinstance(right, int) or isinstance(right, float):
        #   if ctx.op.type == LabeledExprParser.MUL:
        #        return left.replace('"', '') * right

        if isinstance(left, int) or isinstance(left, float):
            if isinstance(right, int) or isinstance(right, float):
                if ctx.op.type == LabeledExprParser.MUL:
                    return left * right
                elif ctx.op.type == LabeledExprParser.MOD:
                    return left % right
                else:
                    return left / right
        else:
            return

    def visitFloorDiv(self, ctx):
        left = self.visit(ctx.expr(0))
        right = self.visit(ctx.expr(1))
        left, right = lrHelper(left, right)
        if isinstance(left, int) or isinstance(left, float):
            if isinstance(right, int) or isinstance(right, float):
                return left // right
        else:
            return

    def visitAddSub(self, ctx):
        left = self.visit(ctx.expr(0))
        right = self.visit(ctx.expr(1))
        left, right = lrHelper(left, right)
        if ctx.op.type == LabeledExprParser.ADD:
            if not isinstance(left, str):
                if not isinstance(right, str):
                    return left + right
            if isinstance(left, str) and isinstance(right, str):
                return left.replace('"', '') + right.replace('"', '')
            else:
                return
        else:
            if isinstance(left, int) or isinstance(left, float):
                if isinstance(right, int) or isinstance(right, float):
                    return left - right
            else:
                return

    def visitEqNeq(self, ctx):
        left = self.visit(ctx.expr(0))
        right = self.visit(ctx.expr(1))
        left, right = lrHelper(left, right)

        if ctx.op.type == LabeledExprParser.EQU:
            if left == right:
                return 1
            return 0
        if left != right:
            return 1
        return 0

    def visitLessGrtr(self, ctx):
        left = self.visit(ctx.expr(0))
        right = self.visit(ctx.expr(1))
        left, right = lrHelper(left, right)

        if ctx.op.type == LabeledExprParser.LESS:
            if left < right:
                return 1
            return 0
        if ctx.op.type == LabeledExprParser.GRTR:
            if left > right:
                return 1
            return 0
        if ctx.op.type == LabeledExprParser.LEQ:
            if left <= right:
                return 1
            return 0
        else:
            if left >= right:
                return 1
            return 0

    def visitNot(self, ctx):
        left = self.visit(ctx.expr())

        if isinstance(left, str):
            try:
                if '.' in left:
                    left = float(left)
                else:
                    left = int(left)
            except ValueError:
                return

        if left == 0:
            return 1
        else:
            return 0

    def visitAndOr(self, ctx):
        left = self.visit(ctx.expr(0))
        right = self.visit(ctx.expr(1))
        left, right = lrHelper(left, right)

        if isinstance(left, str) or isinstance(right, str):
            return

        if ctx.op.type == LabeledExprParser.AND:
            if left and right:
                return 1
            return 0
        else:
            if left or right:
                return 1
            return 0
